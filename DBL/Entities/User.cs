﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBL.Entities
{
    public class User
    {
        public int id { get; set; }
        public string FullName { get; set; }
    }
}

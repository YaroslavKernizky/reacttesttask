﻿using ASP.MVC.React.Control.Configuration;
using ASP.MVC.React.Control.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.MVC.React.Control.Models.ViewModel
{
    public class GetUserComponentModel
    {
        public ProjectConfiguration Config { get; set; }
        public string Users { get; set; }
    }
}
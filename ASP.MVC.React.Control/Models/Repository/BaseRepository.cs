﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP.MVC.React.Control.Models.Repository
{
    public abstract class BaseRepository
    {
        public abstract IQueryable<T> GetItems<T>(Func<T, bool> expression) where T : class;
        public abstract T GetItem<T>(Func<T, bool> expression) where T : class;
        public abstract T Insert<T>(T entity) where T : class;
        public abstract void Update<T>(T entity) where T : class;
        public abstract void Delete<T>(T entity) where T : class;
        public abstract void Save();
    }
}

﻿using System.Data.Entity;
using System.IO;
using System.Web;
using System.Data.SQLite;
using System.Data.SQLite.EF6;
using System.Collections.Generic;
using System;

namespace ASP.MVC.React.Control.Models.Entities
{
    public class ProjectContext: DbContext
    {
        public ProjectContext()
            : base("DefaultConnection")
        {
        }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            int index = this.Database.Connection.ConnectionString.IndexOf('\\');
            int index1 = this.Database.Connection.ConnectionString.IndexOf(';');

            string str = this.Database.Connection.ConnectionString.Substring(index + 1, index1 - index - 1);

            if (!File.Exists(HttpContext.Current.Server.MapPath("~/App_Data/" + str)))
            {
                var initializer = new SQLite.CodeFirst.SqliteDropCreateDatabaseAlways<ProjectContext>(modelBuilder);
                Database.SetInitializer(initializer);
                base.OnModelCreating(modelBuilder);
            }

        }
    }
}

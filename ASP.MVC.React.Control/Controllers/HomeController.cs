﻿using ASP.MVC.React.Control.Configuration;
using ASP.MVC.React.Control.Models.Entities;
using ASP.MVC.React.Control.Models.Repository;
using ASP.MVC.React.Control.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ASP.MVC.React.Control.Controllers
{
    public class HomeController : Controller
    {
        
        private readonly ProjectConfiguration config;
        Repository context;

        public HomeController()
        {
            context = new Repository();
            config = ProjectConfiguration.FromWebConfig(System.Configuration.ConfigurationManager.AppSettings);
        }

        public ActionResult Index()
        {
            GetUserComponentModel model = new GetUserComponentModel();

            var users = context.GetItems<User>(null).ToArray();

            model.Config = config;
            model.Users = new JavaScriptSerializer().Serialize(users);

            return View(model);
        }
    }
}
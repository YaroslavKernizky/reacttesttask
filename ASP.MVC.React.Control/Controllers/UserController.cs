﻿using ASP.MVC.React.Control.Models.Entities;
using ASP.MVC.React.Control.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ASP.MVC.React.Control.Controllers
{
    public class UserController : ApiController
    {
        Repository context = new Repository();

        [HttpGet]
        public IEnumerable<User> GetUserName(string id = "")
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return new User[0];
            }

            IEnumerable<User> users = context.GetItems<User>(u => u.FullName.ToLower().Contains(id.ToLower())).ToArray();

            return users == null ? new User[0] : users;
        }

        [HttpPost]
        public HttpResponseMessage CreateUser([FromBody]User u)
        {
            if (u == null)
            {
                ModelState.AddModelError("Name", "Name is required");
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            User user = new User() { FullName = u.FullName};

            context.Insert<User>(user);
            context.Save();

            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}

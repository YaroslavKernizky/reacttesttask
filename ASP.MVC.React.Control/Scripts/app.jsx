﻿class Item extends React.Component {
    constructor(props) {
        super(props);
        this.onSelected = this.onSelected.bind(this);
    }

    onSelected = (e) => {
        this.props.onSelected(e.target);
    }

    render() {
        return <li onClick={this.onSelected}>{this.props.name}</li>;
    }
}

class SearchPlugin extends React.Component {
    constructor(props) {
        super(props);
        this.onTextChanged = this.onTextChanged.bind(this);
    }

    onTextChanged = (e) => {
        this.props.filter(e.target);
    }

    render() {
        return <input type="text" className="form-control" value={ this.props.value } placeholder="User Name" onChange={this.onTextChanged} />;
    }
}

class ItemsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = { itemsList: [], constList: JSON.parse(this.props.users), value: '' };
        this.text = '';

        this.filterList = this.filterList.bind(this);
        this.onSelected = this.onSelected.bind(this);
    }

    filterList(data) {
        this.setState({ value: data.innerText });
        $.get('http://localhost:5340/api/User/' + data.value.trim(), data => {
            this.setState({ itemsList: !data || !data.length ? [] : data });
        });
    }

    onSelected = (data) => {
        this.setState({ value: data.innerText, isClear: true });
    }

    getSearchPlugin(text = '') {
        if (text.trim() === '') {
            return <SearchPlugin filter={this.filterList} />
        } else {
            this.setState({ itemsList: [] });
            return <SearchPlugin value={text} filter={this.filterList} />
        }
    }

    render() {

        if (this.state.itemsList && this.state.itemsList.length > 0) {
            return <div className="user-container">
                {this.getSearchPlugin(this.state.value)}
                <div className="match-user-names">
                    <ul>
                        {
                            this.state.itemsList.map(item => {
                                return <Item key={item.id} name={item.FullName} onSelected={this.onSelected} />
                            })
                        }
                    </ul>
                </div>;
        </div>;
        } else {
            return <div className="user-container">
                <SearchPlugin filter={this.filterList} />
            </div>;
        }

    }
}

class AddUser extends React.Component {

    constructor(props) {
        super(props);
        this.state = { userName: '' };

        this.data = null;

        this.filterList = this.filterList.bind(this);
        this.save = this.save.bind(this);
    }

    filterList(data) {
        this.data = data;
        this.setState({ userName: data.value.trim() });
    }

    save() {
        if (!this.state.userName || this.state.userName.trim() === '') {
            return;
        }

        data = {
            FullName: this.state.userName
        };

        $.post('http://localhost:5340/api/User', data)
            .done(data => {
                const result = data;
                this.data.value = '';
            })
            .error(error => {
                const err = error;
            });
    }

    render() {
        return <div>
            <button className="btn btn-primary" onClick={this.save} >Add User</button>
            <div className="add-user-name">
                <SearchPlugin filter={this.filterList} />
            </div>
        </div>
    }
}
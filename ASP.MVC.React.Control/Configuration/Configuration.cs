﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.WebPages;

namespace ASP.MVC.React.Control.Configuration
{
    public class ProjectConfiguration
    {
        public static ProjectConfiguration FromWebConfig(NameValueCollection nameValueCollection)
        {
            try
            {
                //Path mapped from URL
                var configuration = new ProjectConfiguration
                {
                    MaxDropDownHeight = nameValueCollection["MaxDropDownHeight"].AsInt(),
                    ItemsLimit = nameValueCollection["ItemsLimit"].AsInt()
                };

                return configuration;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Invalid settings in web.config", ex);
            }
        }

        public int MaxDropDownHeight { get; set; }
        public int ItemsLimit { get; set; }
    }
}